<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Агенство недвижимости</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3"
            crossorigin="anonymous" defer></script>
    <script src="https://code.jquery.com/jquery-3.6.1.min.js"
            integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous" defer></script>
    <script src="js/register.js" defer></script>
</head>
<body>
<div class="d-flex justify-content-center align-items-center vh-100">
    <div class="border border-dark rounded-3 shadow">
        <form class="px-3 py-3" id="registerForm">
            <div class="mb-3">
                <label for="exampleInputName1" class="form-label">ФИО</label>
                <input type="text"  name="full_name" class="form-control shadow-sm" id="exampleInputName1" aria-describedby="emailHelp" placeholder="Введите полное имя">
            </div>
            <div class="mb-3">
                <label for="exampleInputLogin1" class="form-label">Логин</label>
                <input type="text" name="login" class="form-control shadow-sm" id="exampleInputLogin1" aria-describedby="emailHelp" placeholder="Введите логин">
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Email</label>
                <input type="text" name="email" class="form-control shadow-sm" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Введите адрес электронной почты">
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Пароль</label>
                <input type="password" name="password" class="form-control shadow-sm" id="exampleInputPassword1" placeholder="Введите пароль">
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword2" class="form-label">Подтверждение пароли</label>
                <input type="password" name="password_confirm" class="form-control shadow-sm" id="exampleInputPassword2" placeholder="Подтвердите пароль">
            </div>
            <button type="submit" class="w-100 btn btn-primary d-block mx-auto shadow-sm">Регистрация</button>
            <div class="mt-2">У вас уже есть аккаунт? - <a href="/loginForm.php" class="cursor-pointer text-decoration-none">Авторизоваться</a></div>
        </form>
    </div>
</div>
</body>
</html>