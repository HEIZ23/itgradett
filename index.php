<?php

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = explode('/', $uri);

if ((isset($uri[2]) && $uri[2] != 'user') || !isset($uri[3])) {
    header("HTTP/1.1 404 Not Found");
    exit();
}

require __DIR__ . "/Controller/Api/UserController.php";
require __DIR__ . "/authorizationAjaxRequest.php";
require __DIR__ . "/Controller/Api/AdController.php";

switch($uri[3]){
    case 'register':
    case 'login':

        $authorizationAjaxRequest = new authorizationAjaxRequest($_REQUEST);
        $strMethodName = $uri[3];
        $authorizationAjaxRequest->{$strMethodName}();
        break;
    case 'list':
        $UserController = new UserController();
        $strMethodName = $uri[3] . 'Action';
        $UserController->{$strMethodName}();
        break;
    case 'allad':
        $AdController = new AdController();
        $strMethodName = $uri[3] . 'Action';
        $AdController->{$strMethodName}();
        break;
}


