<?php

include_once __DIR__ . "/DB.php";

class Ad extends DB
{
    private $nameObject;
    private $address;
    private $description;
    private $price;

    public function getAllObjects(){
        return $this->selectData('SELECT * FROM ads');
    }
}