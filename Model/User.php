<?php

include_once __DIR__ . "/DB.php";

class User extends DB
{
    protected $id;
    protected $fullName;
    protected $login;
    protected $email;
    protected $password;
    protected $avatar;
    protected $salt;

    private $isAuthorized = false;


    public function auth($login, $password)
    {

        $query = "select id,full_name from users where
            login='{$login}' and password={$password} limit 1";

        $result = $this->selectData($query);
        $salt = $this->getSalt($login);

        if (!$salt) {
            return false;
        }

        $hashes = $this->passwordHash($password, $salt);

        $this->user = $result;

        if (!$this->user) {
            $this->isAuthorized = false;
        } else {
            $this->isAuthorized = true;
            $this->id = $this->user['id'];
        }

        return $this->isAuthorized;
    }


    public function logout()
    {
        if (!empty($_SESSION["user_id"])) {
            unset($_SESSION["user_id"]);
        }
    }

    public function saveSession()
    {
        $_SESSION["user_id"] = $this->id;
    }


    public function create($fullName, $login, $email, $password)
    {
        $hashes = $this->passwordHash($password);
        $query = "insert into users (full_name, login, email, password, salt)
            values ('{$fullName}','{$login}','{$email}','{$hashes['hash']}','{$hashes['salt']}')";

        try {
            $this->conn->beginTransaction();
            $this->conn->query($query);
            $result = $this->conn->commit();
            $id = $this->conn->lastInsertId();
        } catch (\PDOException $e) {
            $this->conn->rollback();
            echo "Database error: " . $e->getMessage();
            die();
        }

        if (!$result) {
            $info = $this->conn->errorInfo();
            printf("Database error %d %s", $info[1], $info[2]);
            die();
        }

        return $id;
    }

    public function getSalt($login)
    {
        $query = "select salt from users where login = '{$login}' limit 1";
        $result = $this->selectData($query);

        if (!$result) {
            return false;
        }

        return $result[0]["salt"];
    }

    public function passwordHash($password, $salt = null, $iterations = 10)
    {
        $salt || $salt = uniqid();
        $hash = md5(md5($password . md5(sha1($salt))));

        for ($i = 0; $i < $iterations; ++$i) {
            $hash = md5(md5(sha1($hash)));
        }

        return ['hash' => $hash, 'salt' => $salt];
    }

    public function getUsers()
    {
        return $this->selectData('SELECT * FROM users');
    }
}