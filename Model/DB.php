<?php

class DB
{
    private $host = "localhost";
    private $user = "root";
    private $pwd = "";
    private $dbName = "itgrade";
    protected $conn;
    private $data = [];

    public function __construct(){
        try {
            $this->conn = new PDO("mysql:host={$this->host}; dbname={$this->dbName}; charset=utf8mb4",$this->user, $this->pwd);
            $this->conn->setAttribute(
                PDO::ATTR_DEFAULT_FETCH_MODE,
                PDO::FETCH_ASSOC
            );
        } catch (PDOException $e){
            echo 'Connection is failed - ' . $e->getMessage();
        }

    }

    public function selectData($query){
        try {
            $doQuery = $this->conn->query($query);
            while($res = $doQuery->fetch(PDO::FETCH_ASSOC)){  $this->data[]=$res;  }
            return $this->data;
        } catch(PDOException $e){
            echo 'Query failed'.$e->getMessage();
        }
    }
}