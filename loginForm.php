<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Агенство недвижимости</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3"
            crossorigin="anonymous" defer></script>
    <script src="https://code.jquery.com/jquery-3.6.1.min.js"
            integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous" defer></script>
    <script src="js/login.js" defer></script>
</head>
<body>
<div class="d-flex justify-content-center align-items-center vh-100">
    <div class="border border-dark rounded shadow">
        <form class="px-3 py-3" id="loginForm">
            <div class="mb-3">
                <label for="exampleInputLogin" class="form-label">Логин</label>
                <input type="text" class="form-control shadow-sm" id="exampleInputLogin" aria-describedby="loginHelp"
                       placeholder="Введите логин" required>
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword" class="form-label">Пароль</label>
                <input type="password" class="form-control shadow-sm" id="exampleInputPassword"
                       placeholder="Введите пароль" required>
            </div>
            <button type="submit" class="w-100 btn btn-primary d-block mx-auto shadow-sm">Войти</button>
            <div class="mt-2">У вас нет аккаунта? - <a href="/register.php" class="cursor-pointer text-decoration-none">Зарегистрироваться</a>
            </div>
        </form>
    </div>
</div>
</body>
</html>