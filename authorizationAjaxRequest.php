<?php

include_once './Model/User.php';
include_once 'AjaxRequest.php';
session_start();

class authorizationAjaxRequest extends AjaxRequest
{
    public $actions = [
        "login" => "login",
        "logout" => "logout",
        "register" => "register",
    ];

    public function login()
    {
        if ($_SERVER["REQUEST_METHOD"] !== "POST") {
            // Method Not Allowed
            http_response_code(405);
            header("Allow: POST");
            $this->setFieldError("main", "Method Not Allowed");
            return;
        }


        $login = $this->getRequestParam('login');
        $password = $this->getRequestParam('password');

        $user = new User();
        $authResult = $user->auth($login, $password);
        var_dump($authResult);
        exit;
        $this->status = 'ok';
        $this->setResponse('redirect', 'https://www.google.com/');
        $this->message = "Hello, {$login}! Access granted.";
    }

    public function logout()
    {
        if ($_SERVER["REQUEST_METHOD"] !== "POST") {
            // Method Not Allowed
            http_response_code(405);
            header("Allow: POST");
            $this->setFieldError("main", "Method Not Allowed");
            return;
        }


        $user = new User();
        $user->logout();

        $this->setResponse("redirect", ".");
        $this->status = "ok";
    }

    public function register()
    {
        if ($_SERVER["REQUEST_METHOD"] !== "POST") {
            // Method Not Allowed
            http_response_code(405);
            header("Allow: POST");
            $this->setFieldError("main", "Method Not Allowed");
            return;
        }
        $fullName = $this->getRequestParam('fullName');
        $login = $this->getRequestParam('login');
        $email = $this->getRequestParam('email');
        $password = $this->getRequestParam('password');

        $user = new User();

        try {
            $new_user_id = $user->create($fullName, $login, $email, $password);
        } catch (\Exception $e) {
            $this->setFieldError("username", $e->getMessage());
            return;
        }
        $user->auth($login, $password);

        $this->message = "Hello, {$fullName}! Thank you for registration.";
        $this->setResponse("redirect", "/");
        $this->setResponse("id", $new_user_id);
        $this->status = "ok";
    }

}

$ajaxRequest = new AuthorizationAjaxRequest($_REQUEST);
$ajaxRequest->showResponse();